# tf.[keras](https://so.csdn.net/so/search?q=keras&spm=1001.2101.3001.7020)介绍

Keras是一个用Python编写的开源神经网络库。它能够运行在TensorFlow，Microsoft Cognitive Toolkit，Theano或PlaidML之上。TensorFlow 1.9 新增 tf.keras,Keras与TF的深度集成。

优点:

Keras 遵循减少认知困难的最佳实践: 它提供一致且简单的 API，它将常见用例所需的用户操作数量降至最低，并且在用户错误时提供清晰和可操作的反馈。

因为 Keras 与底层深度学习语言（特别是 TensorFlow）集成在一起，所以它可以让你实现任何你可以用基础语言编写的东西。特别是，tf.keras 作为 Keras API 可以与 TensorFlow 工作流无缝集成。

Keras 被工业界和学术界广泛采用

![image-20220424223854670](https://gitee.com/king799/image/raw/master/image/image-20220424223854670.png)